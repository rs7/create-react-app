import { createRoot } from 'react-dom/client';

import { Application } from '@/components';

import '@/index.scss';

const container = document.querySelector('#root')

if (container) {
	createRoot(container).render(<Application />)
} else {
	document.body.append(document.createTextNode('Не найден корневой контейнер'))
}
