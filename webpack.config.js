const CopyPlugin = require('copy-webpack-plugin')
const path = require('path')
const ReactFreshBabelPlugin = require('react-refresh/babel')
const ReactRefreshPlugin = require('@pmmmwh/react-refresh-webpack-plugin')

module.exports = {
	devtool: false,

	entry: path.resolve(__dirname, 'src/index.tsx'),

	mode: 'development',

	module: {
		rules: [
			{
				exclude: /node_modules/,
				test: /\.(js|jsx|ts|tsx)$/,
				use: {
					loader: 'babel-loader',
					options: {
						plugins: [ReactFreshBabelPlugin],
						presets: [
							'@babel/preset-env',
							['@babel/preset-react', { runtime: 'automatic' }],
							'@babel/preset-typescript',
						],
					},
				},
			},
			{
				test: /\.(css|scss)$/i,
				use: [
					'style-loader',
					{
						loader: 'css-loader',
						options: {
							modules: {
								auto: true,
								localIdentContext: path.resolve(__dirname, 'src'),
								localIdentName: '[name]__[local]',
							},
						},
					},
					'sass-loader',
				],
			},
		],
	},

	output: {
		filename: 'index.js',
		path: path.resolve(__dirname, 'build/'),
	},

	plugins: [
		new ReactRefreshPlugin(),
		new CopyPlugin({
			patterns: [
				'src/index.html',
			],
		}),
	],

	resolve: {
		alias: {
			'@': path.resolve(__dirname, 'src/'),
		},
		extensions: ['.js', '.jsx', '.ts', '.tsx'],
	},
}
